/****************************************************************************
**
** Copyright (C) 2020 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of TreeView.
**
** $QT_BEGIN_LICENSE:GPL-MARKETPLACE-QT$
**
** Marketplace License Usage
** Users, who have licensed the Software under the Qt Marketplace license
** agreement, may use this file in accordance with the Qt Marketplace license
** agreement provided with the Software or, alternatively, in accordance with
** the terms contained in a written agreement between the licensee and The Qt
** Company. For licensing terms and conditions see
** https://www.qt.io/terms-conditions/#marketplace and
** https://www.qt.io/terms-conditions. For further information use the contact
** form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <qtest.h>

#include <QtQuickTest/quicktest.h>

#include <QtQuick/qquickview.h>
#include <QtQml/qqmlapplicationengine.h>
#include <QtQml/qqmlcontext.h>

#include <QtQuick/private/qquicktableview_p.h>
#include <QtQuick/private/qquicktableview_p.h>
#include <QtQuick/private/qquicktableview_p_p.h>

#include "testmodel.h"

// Note that QQuickTreeView is not directly accessible from this test since
// it's defined inside a QML plugin, and not inside a library we link to.
// But since it inherits from QQuickTableView, we cast it to that instead
#define LOAD_TREEVIEW(FILENAME) \
    QQuickView *view = createView(FILENAME); \
    QVERIFY(QTest::qWaitForWindowActive(view)); \
    auto treeView = view->rootObject()->property("treeView").value<QQuickTableView *>(); \
    QVERIFY(treeView); \
    auto treeViewPrivate = QQuickTableViewPrivate::get(treeView); \
    Q_UNUSED(treeViewPrivate) \
    auto model = treeView->model().value<TestModel *>(); \
    Q_UNUSED(model)

#define WAIT_UNTIL_POLISHED_ARG(item) \
    QVERIFY(QQuickTest::qIsPolishScheduled(item)); \
    QVERIFY(QQuickTest::qWaitForItemPolished(item))
#define WAIT_UNTIL_POLISHED WAIT_UNTIL_POLISHED_ARG(treeView)

// ########################################################

class tst_treeview : public QObject
{
    Q_OBJECT

    QQuickView *createView(const QString &filename);

private slots:
    void showTreeView();
    void expandAndCollapseRoot();
    void expandAndCollapseChildren();
    void expandChildPendingToBeVisible();
    void attachedPropertiesRoot();
    void attachedPropertiesChildren();
    void emptyModel();
    void updatedModifiedModel();
    void keypressOverload();
    void invalidModelIndex();
    void insertRows();
};

QQuickView *tst_treeview::createView(const QString &filename)
{
    QQuickView *window = new QQuickView(nullptr);
    window->setFramePosition(QPoint(100, 100));
    window->setSource(QUrl(QStringLiteral("qrc:///data/") + filename));
    window->show();
    return window;
}

void tst_treeview::showTreeView()
{
    LOAD_TREEVIEW("normaltreeview.qml");
    // Check that the view is showing the root of the tree
    QCOMPARE(treeViewPrivate->loadedRows.count(), 1);
}

void tst_treeview::expandAndCollapseRoot()
{
    LOAD_TREEVIEW("normaltreeview.qml");
    // Check that the view only has one row loaded so far (the root of the tree)
    QCOMPARE(treeViewPrivate->loadedRows.count(), 1);

    // Expand the root
    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 0));
    WAIT_UNTIL_POLISHED;
    // We now expect 5 rows, the root pluss it's 4 children
    QCOMPARE(treeViewPrivate->loadedRows.count(), 5);

    QMetaObject::invokeMethod(treeView, "collapse", Q_ARG(int, 0));
    WAIT_UNTIL_POLISHED;
    // Check that the view only has one row loaded again (the root of the tree)
    QCOMPARE(treeViewPrivate->loadedRows.count(), 1);
}

void tst_treeview::expandAndCollapseChildren()
{
    // Check that we can expand and collapse children, and that
    // the tree ends up with the expected rows
    LOAD_TREEVIEW("editabletreeview.qml");

    const int childCount = 4;

    // Expand the last child of a parent recursively four times
    for (int level = 0; level < 4; ++level) {
        const int nodeToExpand = level * childCount;
        const int firstChildRow = nodeToExpand + 1; // (+ 1 for the root)
        const int lastChildRow = firstChildRow + 4;
        QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, nodeToExpand));
        WAIT_UNTIL_POLISHED;
        QCOMPARE(treeView->rows(), lastChildRow);

        auto childItem1 = treeViewPrivate->loadedTableItem(QPoint(0, firstChildRow))->item;
        QCOMPARE(childItem1->property("text").toString(), "0, 0");
        auto childItem2 = treeViewPrivate->loadedTableItem(QPoint(0, firstChildRow + 1))->item;
        QCOMPARE(childItem2->property("text").toString(), "1, 0");
        auto childItem3 = treeViewPrivate->loadedTableItem(QPoint(0, firstChildRow + 2))->item;
        QCOMPARE(childItem3->property("text").toString(), "2, 0");
    }

    // Collapse down from level 2 (deliberatly not mirroring the expansion by
    // instead collapsing both level 3 and 4 in one go)
    for (int level = 2; level > 0; --level) {
        const int nodeToCollapse = level * childCount;
        const int firstChildRow = nodeToCollapse - childCount + 1;
        const int lastChildRow = nodeToCollapse + 1; // (+ 1 for the root)

        QMetaObject::invokeMethod(treeView, "collapse", Q_ARG(int, nodeToCollapse));
        WAIT_UNTIL_POLISHED;
        QCOMPARE(treeView->rows(), lastChildRow);

        auto childItem1 = treeViewPrivate->loadedTableItem(QPoint(0, firstChildRow))->item;
        QCOMPARE(childItem1->property("text").toString(), "0, 0");
        auto childItem2 = treeViewPrivate->loadedTableItem(QPoint(0, firstChildRow + 1))->item;
        QCOMPARE(childItem2->property("text").toString(), "1, 0");
        auto childItem3 = treeViewPrivate->loadedTableItem(QPoint(0, firstChildRow + 2))->item;
        QCOMPARE(childItem3->property("text").toString(), "2, 0");
    }

    // Collapse the root
    QMetaObject::invokeMethod(treeView, "collapse", Q_ARG(int, 0));
    WAIT_UNTIL_POLISHED;
    QCOMPARE(treeView->rows(), 1);
}

void tst_treeview::expandChildPendingToBeVisible()
{
    // Check that if we expand a row r1, and that row has a child r2 that can
    // be expanded, we can continue to expand c2 immediately, even if r1 is
    // still pending to be shown as expanded in the view.
    LOAD_TREEVIEW("normaltreeview.qml");
    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 0));
    // The view has not yet been updated at this point to show
    // the newly expanded children, so it still has only one row.
    QCOMPARE(treeView->rows(), 1);
    // ...but we still expand row 5, which is a child that has children
    // in the proxy model
    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 4));
    QCOMPARE(treeView->rows(), 1);

    WAIT_UNTIL_POLISHED;

    // Now the view have updated to show
    // all the rows that has been expanded.
    QCOMPARE(treeView->rows(), 9);
}

void tst_treeview::attachedPropertiesRoot()
{
    LOAD_TREEVIEW("normaltreeview.qml");
    QCOMPARE(treeViewPrivate->loadedRows.count(), 1);

    const auto rootFxItem = treeViewPrivate->loadedTableItem(QPoint(0, 0));
    QVERIFY(rootFxItem);
    const auto rootItem = rootFxItem->item;
    QVERIFY(rootItem);

    const auto context = qmlContext(rootItem.data());
    const auto viewProp = context->contextProperty("view").value<QObject *>();
    const auto hasChildren = context->contextProperty("hasChildren").toBool();
    const auto isExpanded = context->contextProperty("isExpanded").toBool();
    const auto depth = context->contextProperty("depth").toInt();

    QCOMPARE(viewProp, treeView);
    QCOMPARE(hasChildren, true);
    QCOMPARE(isExpanded, false);
    QCOMPARE(depth, 0);

    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 0));
    WAIT_UNTIL_POLISHED;

    const auto isExpandedAfterExpand = context->contextProperty("isExpanded").toBool();
    QCOMPARE(isExpandedAfterExpand, true);
}

void tst_treeview::attachedPropertiesChildren()
{
    LOAD_TREEVIEW("normaltreeview.qml");
    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 0));
    WAIT_UNTIL_POLISHED;
    // We now expect 5 rows, the root pluss it's 4 children
    QCOMPARE(treeViewPrivate->loadedRows.count(), 5);

    // The last child has it's own children, so expand that one as well
    const auto rootIndex = model->index(0, 0);
    const int childCount = model->rowCount(rootIndex);
    QCOMPARE(childCount, 4);
    const auto lastChildIndex = model->index(childCount - 1, 0, rootIndex);
    QVERIFY(lastChildIndex.isValid());
    QCOMPARE(model->hasChildren(lastChildIndex), true);

    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 4));
    WAIT_UNTIL_POLISHED;
    // We now expect root + 4 children + 4 children = 9 rows
    const int rowCount = treeViewPrivate->loadedRows.count();
    QCOMPARE(rowCount, 9);

    // Go through all rows in the view, except the root
    for (int row = 1; row < rowCount; ++row) {
        const auto childFxItem = treeViewPrivate->loadedTableItem(QPoint(0, row));
        QVERIFY(childFxItem);
        const auto childItem = childFxItem->item;
        QVERIFY(childItem);

        const auto context = qmlContext(childItem.data());
        const auto viewProp = context->contextProperty("view").value<QObject *>();
        const auto isExpanded = context->contextProperty("isExpanded").toBool();
        const auto depth = context->contextProperty("depth").toInt();
        const auto hasChildren = context->contextProperty("hasChildren").toBool();

        QCOMPARE(viewProp, treeView);
        QCOMPARE(depth, row <= 4 ? 1 : 2);
        QCOMPARE(isExpanded, row == 4);
        QCOMPARE(hasChildren, row == 4 || row == 8);
    }
}

void tst_treeview::emptyModel()
{
    // Check that you can assign an empty model, and that
    // calling functions will work as expected (and at least not crash)
    LOAD_TREEVIEW("normaltreeview.qml");
    treeView->setModel(QVariant());
    treeView->forceActiveFocus();
    WAIT_UNTIL_POLISHED;

    QCOMPARE(treeViewPrivate->loadedItems.count(), 0);
    QCOMPARE(treeView->rows(), 0);
    QCOMPARE(treeView->columns(), 0);

    // Check that we don't crash:
    QTest::keyEvent(QTest::Click, treeView->window(), Qt::Key_Up);
    QTest::keyEvent(QTest::Click, treeView->window(), Qt::Key_Down);
    QTest::keyEvent(QTest::Click, treeView->window(), Qt::Key_Left);
    QTest::keyEvent(QTest::Click, treeView->window(), Qt::Key_Right);

    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 0));
    QMetaObject::invokeMethod(treeView, "collapse", Q_ARG(int, 0));
    QMetaObject::invokeMethod(treeView, "toggleExpanded", Q_ARG(int, 0));

    int depth;
    bool hasChildren, hasSiblings, isExpanded;
    QMetaObject::invokeMethod(treeView, "depth", Q_RETURN_ARG(int, depth), Q_ARG(int, 0));
    QMetaObject::invokeMethod(treeView, "hasChildren", Q_RETURN_ARG(bool, hasChildren), Q_ARG(int, 0));
    QMetaObject::invokeMethod(treeView, "hasSiblings", Q_RETURN_ARG(bool, hasSiblings), Q_ARG(int, 0));
    QMetaObject::invokeMethod(treeView, "isExpanded", Q_RETURN_ARG(bool, isExpanded), Q_ARG(int, 0));
    QCOMPARE(depth, -1);
    QCOMPARE(hasChildren, false);
    QCOMPARE(hasSiblings, false);
    QCOMPARE(isExpanded, false);
}

void tst_treeview::updatedModifiedModel()
{
    LOAD_TREEVIEW("editabletreeview.qml");
    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 0));
    WAIT_UNTIL_POLISHED;
    // We now expect 5 rows, the root plus it's 4 children
    QCOMPARE(treeViewPrivate->loadedRows.count(), 5);

    auto rootFxItem = treeViewPrivate->loadedTableItem(QPoint(0, 0));
    QVERIFY(rootFxItem);
    auto rootItem = rootFxItem->item;
    QVERIFY(rootItem);
    QCOMPARE(rootItem->property("text"), "0, 0");
    model->setData(model->index(0, 0), QVariant(QString("Changed")));
    QCOMPARE(rootItem->property("text"), "Changed");

    auto rootFxItemCol1 = treeViewPrivate->loadedTableItem(QPoint(1, 2));
    QVERIFY(rootFxItemCol1);
    auto rootItemCol1 = rootFxItemCol1->item;
    QVERIFY(rootItemCol1);
    QCOMPARE(rootItemCol1->property("text"), "1, 1");
    model->setData(model->index(1, 1, model->index(0,0)), QVariant(QString("Changed")));
    QCOMPARE(rootItemCol1->property("text"), "Changed");
}

void tst_treeview::keypressOverload()
{
    // Check that TreeView only eats the key events it uses
    LOAD_TREEVIEW("keypress.qml");
    treeView->forceActiveFocus();
    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 0));
    WAIT_UNTIL_POLISHED;

    QTest::keyEvent(QTest::Click, treeView->window(), Qt::Key_Down);
    int treeViewPressed = view->rootObject()->property("treeViewPressed").value<int>();
    int parentPressed = view->rootObject()->property("parentPressed").value<int>();
    QCOMPARE(treeViewPressed, Qt::Key_Down);
    QCOMPARE(parentPressed, Qt::Key(0));

    QTest::keyEvent(QTest::Click, treeView->window(), Qt::Key_A);
    treeViewPressed = view->rootObject()->property("treeViewPressed").value<int>();
    parentPressed = view->rootObject()->property("parentPressed").value<int>();
    QCOMPARE(treeViewPressed, Qt::Key_A);
    QCOMPARE(parentPressed, Qt::Key_A);
}

void tst_treeview::invalidModelIndex()
{
    // Check that you can call pass an invalid QModelIndex to the
    // TreeView API without causing a crash / assert.
    LOAD_TREEVIEW("normaltreeview.qml");
    QMetaObject::invokeMethod(treeView, "isModelIndexExpanded", Q_ARG(QModelIndex, QModelIndex()));
    QMetaObject::invokeMethod(treeView, "collapseModelIndex", Q_ARG(QModelIndex, QModelIndex()));
    QMetaObject::invokeMethod(treeView, "expandModelIndex", Q_ARG(QModelIndex, QModelIndex()));
    QMetaObject::invokeMethod(treeView, "toggleModelIndexExpanded", Q_ARG(QModelIndex, QModelIndex()));
    QMetaObject::invokeMethod(treeView, "itemAtIndex", Q_ARG(QModelIndex, QModelIndex()));
    QMetaObject::invokeMethod(treeView, "itemAtModelIndex", Q_ARG(QModelIndex, QModelIndex()));
    QMetaObject::invokeMethod(treeView, "mapToModel", Q_ARG(QModelIndex, QModelIndex()));
    QMetaObject::invokeMethod(treeView, "mapFromModel", Q_ARG(QModelIndex, QModelIndex()));
}

void tst_treeview::insertRows()
{
    // Check that if we add new rows to the model, TreeView gets updated
    // to contain the new expected number of rows (flattened to a list)
    LOAD_TREEVIEW("editabletreeview.qml");
    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 0));
    WAIT_UNTIL_POLISHED;

    QCOMPARE(treeView->rows(), 5);

    const QModelIndex rootNode = model->index(0, 0, QModelIndex());
    model->insertRows(0, 2, rootNode);
    WAIT_UNTIL_POLISHED;

    QCOMPARE(treeView->rows(), 7);
    auto childItem1 = treeViewPrivate->loadedTableItem(QPoint(0, 1))->item;
    QCOMPARE(childItem1->property("text").toString(), "0, 0 (inserted)");
    auto childItem2 = treeViewPrivate->loadedTableItem(QPoint(0, 2))->item;
    QCOMPARE(childItem2->property("text").toString(), "1, 0 (inserted)");
    auto childItem3 = treeViewPrivate->loadedTableItem(QPoint(0, 3))->item;
    QCOMPARE(childItem3->property("text").toString(), "0, 0");

    const QModelIndex indexOfInsertedChild = model->index(1, 0, rootNode);
    model->insertRows(0, 2, indexOfInsertedChild);
    QMetaObject::invokeMethod(treeView, "expand", Q_ARG(int, 2));
    WAIT_UNTIL_POLISHED;

    QCOMPARE(treeView->rows(), 9);
}

QTEST_MAIN(tst_treeview)

#include "tst_treeview.moc"
