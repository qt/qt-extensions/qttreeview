TEMPLATE = app
TARGET = tst_treeview
CONFIG += testcase

CONFIG += qmltypes
QML_IMPORT_NAME = TestModel
QML_IMPORT_MAJOR_VERSION = 1

QT += qml testlib quick quick-private qmltest

SOURCES += \
    tst_treeview.cpp \
    testmodel.cpp

OTHER_FILES += $$files($$PWD/data)
RESOURCES += $$OTHER_FILES

HEADERS += \
    testmodel.h
