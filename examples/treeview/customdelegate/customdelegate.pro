requires(qtHaveModule(widgets))

TEMPLATE = app

QT += gui qml quick widgets quickcontrols2

RESOURCES += \
    main.qml \
    CustomTreeView.qml \
    folderopen.png \
    folderclosed.png \
    file.png

SOURCES += \
    main.cpp \

target.path = $$[QT_INSTALL_EXAMPLES]/treeview/customdelegate
INSTALLS += target
