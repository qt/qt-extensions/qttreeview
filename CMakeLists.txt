# Generated from treeview.pro.

cmake_minimum_required(VERSION 3.15.0)

set(QT_NO_INTERNAL_COMPATIBILITY_FUNCTIONS TRUE)

include(.cmake.conf)
project(QtTreeView
    VERSION "${QT_REPO_MODULE_VERSION}"
    DESCRIPTION "Qt Quick Treeview plugin"
    HOMEPAGE_URL "https://marketplace.qt.io/products/treeview"
    LANGUAGES CXX C
)

find_package(Qt6 ${PROJECT_VERSION} CONFIG REQUIRED COMPONENTS BuildInternals Core Quick
    QuickControls2 QuickTest REQUIRED)
find_package(Qt6 ${PROJECT_VERSION} CONFIG OPTIONAL_COMPONENTS Widgets)

if(NOT TARGET Qt::Quick)
    message(NOTICE "Skipping the build as the condition \"TARGET Qt::Quick\" is not met.")
    return()
endif()
if(NOT TARGET Qt::QuickControls2)
    message(NOTICE "Skipping the build as the condition \"TARGET Qt::QuickControls2\" is not met.")
    return()
endif()
qt_build_repo()
